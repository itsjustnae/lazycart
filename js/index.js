// Disables back history
function preventBack() {
    window.history.forward();
}
setTimeout("preventBack()", 0);
window.onunload = function () { null };

// Login function
function userlogin(form) {
    if (form.text.value == "admin") {
        if (form.pass.value == "0000") {
            location = "admin_dashboard.html"
        } else {
            alert("Invalid Password")
        }
    } else {
        alert("Invalid UserID")
    }
}
// Logout function
function logOut() {
    console.log("Admin Logged Out.");
    window.location.href = "index.html";
}
//Delete Table Row
function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
}

// Search data function
function searchTable() {

    // Declare variables 
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value;
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

// Print Data to PDF or Notebook
function printData() {
    var tabToPrint = document.getElementById("printTable");
    newWin = window.open("");
    newWin.document.write(tabToPrint.outerHTML);
    newWin.print();
    newWin.close();
}
// Invalid Request
function invalidRequest() {
    alert("Invalid Request.");
}
